param(
    [string]$uri="http://UKATMS01/WS_XMS_WEB/awws/WS_XMS.awws?wsdl",
    [string]$file =".\sampleXml copy 3.xml",
    [string]$outfile = "$psscriptroot\Logs\KLS_genere_Expe_$(Get-Date -Format "%yMMdd_HHmmssfff").log"
)

function convertObjTo-str{
  param(
     $myOjb
  )
     $MesValeurs=@()
     foreach ($element in $myOjb) {
        $myProperties = $element | Get-Member -MemberType Property 
        foreach($Propertie in $myProperties){
           $nom=$Propertie.Name
           $valeur=$element.$($nom)
           $MesValeurs += "$nom : $valeur"
        }
     }
  return $MesValeurs
  }

function replaceSpecCar{ 
  param($text)
  $newtext=@()
  $aRemplacer=@("&")
  foreach($car in $aRemplacer){
    foreach($ligne in $text){
      $newligne= $ligne -replace  $car,''
      $newtext+=$newligne
    }
  }
  return $newtext
}

Import-Module "$PSscriptRoot\convertfrom-xml.psm1"


$outfile=$file.Replace(".xml",".log")
$trace = "$psscriptroot\Logs\KLS_genere_Expe_$(Get-Date -Format "%yMMdd_HHmmssfff").log"

#lit le fichier xml géré (dossier TMP)

[xml]$xmlfile=replacespecCar(Get-Content $file)
# instancie le webservice Ukal
$wsKLS = New-WebServiceProxy -Uri $uri 

# renseigne les valeurs sur les objets
$myLogin= ConvertFrom-XML -InputObject $xmlfile.Envelope.Body.KLS_Genere_Expe.login
$myExpedtion= ConvertFrom-XML -InputObject $xmlfile.Envelope.Body.KLS_Genere_Expe.expedition
$myContenant=@()
foreach($contenant in $xmlfile.Envelope.Body.KLS_Genere_Expe.contenant){
  $myContenant+=convertfrom-xml -InputObject $contenant
}
# appelle la méthode kls_génère_Expe


try{
$messageRetour=$wsKLS.KLS_Genere_Expe($myLogin,$myExpedtion,$myContenant) 
$code=0

}
Catch{
  $messageRetour = $_.Exception.Message
  $code = 1 
}

$messageRetour | Out-String -Stream  | Out-File -FilePath $trace -Encoding utf8
$mycolis=@()
foreach($colis in $messageRetour.Contenant){
  $myColis+=convertObjTo-str $colis
}
#$cplmessage= convertObjTo-str $myColis 
ADD-content -path $trace -value $mycolis
#write-host $messageRetour | Out-String -Stream 
#Write-Host $cplmessage
#Write-Host $outfile
ADD-content -path $outfile -value "num_err: $($messageRetour.num_err)" 
ADD-content -path $outfile -value "libelle: $($messageRetour.libelle)"
ADD-content -path $outfile -value "Transporteur: $($messageRetour.Transporteur)"
ADD-content -path $outfile -value "DateLivestimee: $($messageRetour.DateLivestimee)"
ADD-content -path $outfile -value "CoutHA: $($messageRetour.CoutHA)"

$url=""
foreach($contenant in $messageRetour.Contenant){
  if($url.Length-eq 0){
    $url=$messageRetour.Contenant.Url
    ADD-content -path $outfile -value "Url: $url "
    
  }
  #if($url -ne $messageRetour.Contenant.Url){
  #  ADD-content -path $outfile -value "Url: $url "
  #}
}



