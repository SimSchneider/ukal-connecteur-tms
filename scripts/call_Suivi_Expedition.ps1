﻿param(
    [string]$uri="http://UKATMS01/WS_XMS_WEB/awws/WS_XMS.awws?wsdl",
    [string]$typeNo ="nRef",
    [string]$nRefC ="L2012UKA0011" # = numéro de BL
)

Import-Module "$PSscriptRoot\convertfrom-xml.psm1"

function convertObjTo-str{
   param(
      $myOjb
   )
      $MesValeurs=@()
      foreach ($element in $myOjb) {
         $myProperties = $element | Get-Member -MemberType Property 
         foreach($Propertie in $myProperties){
            $nom=$Propertie.Name
            $valeur=$element.$($nom)
            $MesValeurs += "$nom : $valeur"
         }
      }
   return $MesValeurs
   }

function myUTF8 {
   param (
      [string]$mystring
   )
   $enc=[System.Text.Encoding]::default.getbytes($mystring)
   return [System.Text.Encoding]::UTF8.GetString($enc)
}

function Remove-StringLatinCharacters
{
    PARAM ([string]$String)
    [Text.Encoding]::ASCII.GetString([Text.Encoding]::GetEncoding("Cyrillic").GetBytes($String))
}


$uri

# instancie le webservice Ukal
$wsKLS = New-WebServiceProxy -Uri "http://UKATMS01/WS_XMS_WEB/awws/WS_XMS.awws?wsdl"

[xml]$xmlfile = @('
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:WS_XMS">
   <soapenv:Header/>
   <soapenv:Body>
      <urn:suivi_Expedition>
         <login>
            <utilisateur>UKAL</utilisateur>
            <mdp>9Hf@y8</mdp>
            <imprimante_etiquette></imprimante_etiquette>
            <imprimante_laser></imprimante_laser>
         </login>
         <reference>
            <type>{0}</type>
            <number>{1}</number>
         </reference>
         <language>FR</language>
      </urn:suivi_Expedition>
   </soapenv:Body>
</soapenv:Envelope>
') -f  $typeNo,$nRefC

$mylogin= ConvertFrom-XML($xmlfile.Envelope.Body.suivi_Expedition.login)
$myreference= ConvertFrom-XML($xmlfile.Envelope.Body.suivi_Expedition.reference)
#$myLang= ConvertFrom-XML($xmlfile.Envelope.Body.suivi_Expedition.language)

# appelle la méthode suivi_Expedition
$date=Get-Date -Format "%yMMdd_HHmmssfff"

try{
$messageRetour=$wsKLS.suivi_Expedition($myLogin,$myreference,$myLang) 
$code=0
}
Catch{
  $messageRetour = $_.Exception.Message
  $code = 1 
}

$outfile= "$psscriptroot\Logs\suivi_Expedition_$date.log"
$monMessageString =convertObjTo-str $messageRetour | Out-String -Stream  
$monMessageString| Out-File -FilePath $outfile -Encoding utf8
$myColis=$messageRetour.colis
$cplmessage= convertObjTo-str $myColis | Out-String -Stream  
ADD-content -path $outfile -value $cplmessage

$enc = [System.Text.Encoding]::UTF8
$consumerkey ="xvz1evFS4wEEPTGEFPHBog"
$encconsumerkey= $enc.GetBytes($consumerkey)

if($messageRetour.num_err -eq 0){
   Write-Host "Transporteur : $($messageRetour.transporteur)"
   write-host "Service: $($messageRetour.service)"
   foreach($colis in $messageRetour.colis){
      Write-Host "numSuivi  : $($colis.url  )"
      Write-Host "dernierEvenement : $(Remove-StringLatinCharacters($colis.dernierEvenement) )"
      Write-Host "libelleStatut : $(Remove-StringLatinCharacters($colis.libelleStatut ))"
      Write-Host "DateLivraison : $($colis.DateLivraison.Substring(7,2))/$($colis.DateLivraison.Substring(5,2))/$($colis.DateLivraison.Substring(1,4) )"
      Write-Host ""
   }
}
else {
   write-host "Erreur Webservice KLS: $($monMessageString.libelle)"
}

exit $code

